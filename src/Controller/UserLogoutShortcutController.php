<?php

namespace Drupal\user_logout_shortcut\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * {@inheritdoc}
 */
class UserLogoutShortcutController extends ControllerBase {

  /**
   * The Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->loggerFactory = $logger_factory->get('user_logout_shortcut');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function userlogout() {
    drupal_flush_all_caches();
    user_logout();
    $logout_message = $this->t('You have successfully logged out!');
    $this->loggerFactory->notice($logout_message);
    return new JsonResponse(['data' => $logout_message]);
  }

}
