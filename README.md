---------------
# user_logout_shortcut
---------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

This User Logout Shortcut module provides an easy keyboard shortcut hotkey way to logout from user session.

Just press ALT+l to User Logout

RECOMMENDED MODULES
-------------------

 * No extra module is required.

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

	To enable user_logout_shortcut:

	1. Enable the module
	2. Go to the link for manual logout (shortcut/logout) and you will get JSON response. (OR)
	3. Just press ALT+l for an easy keyboard shortcut hotkey way to logout from user session.
	3. Nothing else :)

TROUBLESHOOTING
---------------

 * User Logout Shortcut module doesn't provide any visible functions to the user on its own, it
   just provides keyboard shortcut.


MAINTAINERS
-----------

Current maintainers:

 * Manikandan Ramakrishnan (https://www.drupal.org/user/3508408/)

