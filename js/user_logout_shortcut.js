/**
 * @file
 * JavaScript file for the user_logout_shortcut module.
 */
 (function($, Drupal, drupalSettings) {
	'use strict';
	Drupal.behaviors.user_logout_shortcut = {
		attach: function() {
			$('body').once('user_logout_shortcut').each(function() {
				var path = Drupal.url(drupalSettings.user_logout_shortcut.image_path);
				var imageUrl = '<img class="user-logout logout-center" src="' + path + '"/>';
				// Key events.
				$(document).keydown(function(event) {
					if (event.altKey === true && event.keyCode === 76) {
						$('body').prepend(imageUrl);

						$.ajax({
							url: Drupal.url('shortcut/logout'),
							type: "POST",
							beforeSend: function(xhr) {
								xhr.setRequestHeader('X-Requested-With', {
									toString: function() {
										return '';
									}
								});
							},
							success: function(data) {
								$('body').prepend('<div class="ul-overlay"><div class="ul-popup"><span class="ul-h2">'+JSON.stringify(data.data)+'</span><a class="ul-close" href="#">&times;</a></div></div>');
								$('.ul-overlay .ul-close').click(function() {
									$('.ul-overlay').fadeOut();
									window.location.href = "/";
								});
								setTimeout(function() {
									$('.ul-overlay').fadeOut();
									window.location.href = "/";
								}, 5000);
							},
							error: function(XMLHttpRequest, textStatus) {
								if (XMLHttpRequest.status === 403 || XMLHttpRequest.status === 404) {
									window.location.href = "/";
								}
							}
						});
					}
				});
			});
		}
	};

})(jQuery, Drupal, drupalSettings);